from tkinter import *
from initcanvas import affichageCanvas
from animation import copyTree,changeGrayToWhite,changeRedToGray,copyT1toT0,InitTab
import random
def animationR2(ligne,colonne,taille,canvasListe,canvas):
    canvasListeT1=InitTab(ligne,colonne)
    copyTree(canvasListe,canvasListeT1,ligne,colonne)
    changeGrayToWhite(canvasListe,canvasListeT1,ligne,colonne)
    changeRedToGray(canvasListe,canvasListeT1,ligne,colonne)
    changeGreenToRedProbability(canvasListe,canvasListeT1,ligne,colonne)
    affichageCanvas(canvas,ligne,colonne,taille,canvasListeT1)
    copyT1toT0(canvasListe,canvasListeT1,colonne,ligne)
def changeGreenToRedProbability(canvasListe,canvasListeT1,ligne,colonne):
    for ligneIter in range(0,ligne):
        for colonneIter in range(0,colonne):
            compteur=0
            if(canvasListe[colonneIter][ligneIter]=="Green"):
                if(colonneIter>0):
                    if(canvasListe[colonneIter-1][ligneIter]=="Red"):
                        compteur+=1
                if(colonneIter<colonne-1):
                    if(canvasListe[colonneIter+1][ligneIter]=="Red"):
                        compteur+=1
                if(ligneIter>0):
                    if(canvasListe[colonneIter][ligneIter-1]=="Red"):
                        compteur+=1
                if(ligneIter<ligne-1):
                    if(canvasListe[colonneIter][ligneIter+1]=="Red"):
                        compteur+=1
                if(probabilityToFire(compteur)==True):
                    canvasListeT1[colonneIter][ligneIter]="Red"
def probabilityToFire(compteur):
    probability=1-(1/(compteur+1))
    value=random.randint(1,100)
    if(value<=(probability*100)):
        return True;
    else:
        return False;