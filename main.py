from tkinter import *
import argparse
from initcanvas import initColor,affichageCanvas
from animation import animationR1
from animation2 import animationR2
import time
parser = argparse.ArgumentParser()
parser.add_argument("-rows",help="Nombre de ligne de la grille",type=int,default=10)
parser.add_argument("-cols",help="Nombre de colonne de la grille",type=int,default=10)
parser.add_argument("-cell_size",help="Taille d'une cellule",type=int,default=20)
parser.add_argument("-anim_time",help="durée entre chaque animation",default=500)
parser.add_argument("-afforestation",help="Taux de d'afforestation",default=0.8)
args = parser.parse_args()
ligne=args.rows
colonne=args.cols
taille=args.cell_size
animation=args.anim_time
afforestation=args.afforestation
canvasListe=initColor(ligne,colonne,afforestation)
def click_callback(event):
    x1=event.x/taille
    y1=event.y/taille
    x1=int(x1)
    y1=int(y1)
    if(canvasListe[y1][x1]=="Green"):
        canvas.create_rectangle(x1*taille,y1*taille,(x1+1)*taille,(y1+1)*taille,fill="Red")
        canvasListe[y1][x1]="Red"
def anim():
    animationR1(ligne,colonne,taille,canvasListe,canvas)
    master.after(animation,anim)
def anim2():
    animationR2(ligne,colonne,taille,canvasListe,canvas)
    master.after(animation,anim2)
master=Tk()
canvas = Canvas(master,width=colonne*taille,height=ligne*taille)
buttonFrame=Frame(master)
buttonFrame.pack(side=TOP)
canvas.pack()
boutonR1=Button(buttonFrame,text="Regle n°1",command=anim).pack(side=LEFT,padx=5,pady=5)
boutonR2=Button(buttonFrame,text="Regle n°2",command=anim2).pack(side=LEFT,padx=5,pady=5)
canvas.bind('<Button-1>',click_callback)
affichageCanvas(canvas,ligne,colonne,taille,canvasListe)
master.mainloop()
