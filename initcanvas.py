import random
from tkinter import *
def initColor(ligne,colonne,afforestation):
    canvasLigne=[]
    for ligneIter in range(0,ligne):
        canvasColonne=[]
        for colonneIter in range(0,colonne):
            if(afforestationProbability(afforestation)==True):
                canvasColonne.append("Green")
            else:
                canvasColonne.append("White")
        canvasLigne.append(canvasColonne)
    return canvasLigne
def afforestationProbability(afforestation):
    value=random.randint(1,100)
    if(value<=(afforestation*100)):
        return True;
    else:
        return False;
def affichageCanvas(canvas,ligne,colonne,taille,canvasListe):
    for ligneIter in range(0,ligne):
        for colonneIter in range(0,colonne):
            canvas.create_rectangle(ligneIter*taille,colonneIter*taille,(ligneIter+1)*taille,(colonneIter+1)*taille,fill=canvasListe[colonneIter][ligneIter])