from tkinter import *
from initcanvas import affichageCanvas
import time
def animationR1(ligne,colonne,taille,canvasListe,canvas):
    canvasListeT1=InitTab(ligne,colonne)
    copyTree(canvasListe,canvasListeT1,ligne,colonne)
    changeGrayToWhite(canvasListe,canvasListeT1,ligne,colonne)
    changeRedToGray(canvasListe,canvasListeT1,ligne,colonne)
    changeGreenToRed(canvasListe,canvasListeT1,ligne,colonne)
    affichageCanvas(canvas,ligne,colonne,taille,canvasListeT1)
    copyT1toT0(canvasListe,canvasListeT1,colonne,ligne)
def copyTree(canvasListe,canvasListeT1,ligne,colonne):
    for iteration in range(0,ligne):
        for interation2 in range(0,colonne):
            canvasListeT1[iteration][interation2]=canvasListe[iteration][interation2]

def InitTab(ligne,colonne):
    liste=[]
    for iteration in range(0,ligne):
        listeSecond=[]
        for interation2 in range(0,colonne):
            listeSecond.append("White")
        liste.append(listeSecond)
    return liste
def copyT1toT0(canvasListe,canvasListeT1,colonne,ligne):
    for ligneIter in range(0,ligne):
        for colonneIter in range(0,colonne):
            canvasListe[colonneIter][ligneIter]=canvasListeT1[colonneIter][ligneIter]
def changeGrayToWhite(canvasListe,canvasListeT1,ligne,colonne):
    for ligneIter in range(0,ligne):
        for colonneIter in range(0,colonne):
            if(canvasListe[colonneIter][ligneIter]=="Gray"):
                canvasListeT1[colonneIter][ligneIter]="White"

def changeRedToGray(canvasListe,canvasListeT1,ligne,colonne):
    for ligneIter in range(0,ligne):
        for colonneIter in range(0,colonne):
            if(canvasListe[colonneIter][ligneIter]=="Red"):
                canvasListeT1[colonneIter][ligneIter]="Gray"

def changeGreenToRed(canvasListe,canvasListeT1,ligne,colonne):
    for ligneIter in range(0,ligne):
        for colonneIter in range(0,colonne):
            if(canvasListe[colonneIter][ligneIter]=="Red"):
                if(colonneIter>0):
                    if(canvasListe[colonneIter-1][ligneIter]=="Green"):
                        canvasListeT1[colonneIter-1][ligneIter]="Red"
                if(colonneIter<colonne-1):
                    if(canvasListe[colonneIter+1][ligneIter]=="Green"):
                        canvasListeT1[colonneIter+1][ligneIter]="Red"
                if(ligneIter>0):
                    if(canvasListe[colonneIter][ligneIter-1]=="Green"):
                        canvasListeT1[colonneIter][ligneIter-1]="Red"
                if(ligneIter<ligne-1):
                    if(canvasListe[colonneIter][ligneIter+1]=="Green"):
                        canvasListeT1[colonneIter][ligneIter+1]="Red"

